# INTRO
This repo holds only a `docker-compose.yml` file. Running that file allows to:
- install a prestashop instance locally
- install a Postgres DB locally (it is needed for prestashop to run correctly)
- install a PHPmyadmin instance in `http://localhost:8081`. It just helps to manage the Postgres DB

All those 3 are run as a docker containers.

# TO RUN PRESTASHOP
1. Clone this repository to your local
2. Navigate to the main folder
3. Open the terminal and run `docker-compose up -d`. Please keep in mind that your local machine needs to have `docker` and `docker-compose` installed
4. Wait until the containers are created and started
5. Visit `http://localhost:9876/install123`
    1. Install the Prestashop by moving thorough the installation wizard
    2. In the 'Store Information' screen remeber the email and password passed there. It will be needed to setup the API key for testing purpouses
    3. The general fields and data does not really matter. Except the screen with database connection. Fill it with following (see screenshot): ![DB connection](chrome_s5Fr9kQL15.png)
    4. Continue with the installation
6. When the installation is successful then the shop should be accessible in `http://localhost:9876`. The admin part is visible in `http://localhost:/admin123` with the credentials passed during the shop installation.
7. Now to generate the api key needed for testing: 
    1. Go to the admin panel and login with the credentials passed during the installation
    2. From the left side panel select `Advanced Paramters > Webservices`
    3. Set to `YES` checkbox named `Enable prestashop's webservice` and hit `Save`
    4. In the top right corner click `Add new webservice key`
    5. Click `Generate!` (the key should be generated then) > set status to `YES` > Select ALL of the checkboxes for all of the enpoints and methods
    6. Hit `Save` and the key should be saved
    7. Copy the generaed key value
    8. Find any base64 encode site and encode the key as the following: <your_key>: (see the colon at the end, it is needed) 
